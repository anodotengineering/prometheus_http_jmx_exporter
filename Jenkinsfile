node ('build') {

    env.APP_VERSION=getAppVersion()

    stage('Checkout') {
        checkout scm
    }

    stage("Publish helm"){
        buildHelm()
    }
}


void buildHelm(){
    dir("${WORKSPACE}/deployment/helm/prometheus_http_jmx_exporter"){
    withEnv(["AWS_REGION=us-east-1"]) {
        //https://github.com/helm/helm/issues/1732
        sh "helm init --client-only"
        sh "helm lint ."
        sh "helm plugin install https://github.com/hypnoglow/helm-s3.git || echo 'failed to install plugin...ignoring error' "
        sh "helm repo add anodot s3://anodot-helm-charts/charts"

        def valuesYaml = readYaml file: 'values.yaml'

        echo "The Values $valuesYaml"
        sh "mv values.yaml values.yaml.org"
        writeYaml file: 'values.yaml', data: valuesYaml

        if (fileExists('requirements.yaml')){
            sh "helm dep update ."
        }

        upgradeChartVersion()
        def chartYaml = readYaml file: 'Chart.yaml'
        chartVersion = chartYaml.version

        sh "helm package ."
        sh "helm s3 push --force ./prometheus_http_jmx_exporter-${chartVersion}.tgz anodot"
        }
    }
}



void upgradeChartVersion() {
        def chartYaml = readYaml file: 'Chart.yaml'
        chartYaml.appVersion = "${APP_VERSION}"
        chartYaml.version = "${APP_VERSION}"

        sh "mv Chart.yaml Chart.yaml.org"
        writeYaml file: 'Chart.yaml', data: chartYaml
}

// to make sure that build from other branches will not override master builds
String getAppVersion(def chartVersion){
    def chartYaml = readYaml file: '${WORKSPACE/deployment/helm/prometheus_http_jmx_exporter/Chart.yaml'
    def chartVersion = chartYaml.version

    if ("master" == "${BRANCH_NAME}"){
        return chartVersion
    }else {
        return "${BRANCH_NAME}-${chartVersion}"
    }
}